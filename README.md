# vim-undead
A fork of [vim-undead](https://github.com/chrisduerr/vim-undead) that is less bright, uses 256 colors, and has
more differentiation.

### Installation with vim-plug:
```
Plug 'https://gitlab.com/majick/vim-undead.git'
colorscheme undead
```
